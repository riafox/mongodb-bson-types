/**
 * BSON Converter
 * Contains the static methods for converting data from mongo BSON formats to standard formats
 */
export class BSONConverter {
    /* Depreciated */
    public static mongoId(id:any):string {
        return this.objectId(id);
    }

    /**
     * Converts an ObjectId object to a usable string.
     * Returns null if invalid
     * @param id {any}
     */
    public static objectId(id:any):string {
        if(id.oid) return id.oid;
        if(id.$oid) return id.$oid;
        if(id.$id) return id.$id;
        if(typeof id == 'string') return id;
        return null;
    }

    /**
     * Converts a UTCDateTime object to milliseconds
     * Returns null if invalid
     * @param dateTime {any}
     */
    public static UTCDateTime(dateTime:any):number {
        if(dateTime.$date) {
            if(dateTime.$date.$numberLong) return +dateTime.$date.$numberLong;
        }
        if(dateTime.milliseconds) return +dateTime.milliseconds;
        return null;
    }
}