<a name="0.1.5"></a>
# [0.1.5](https://bitbucket.org/riafox/mongodb-bson-types/commits/f3d3056eb54f28438a24d5a81761674380deab6f) (2017-05-22)

### Bug Fixes
* Fixes module resolution and typing resolutions for Angular AOT compatibility

<a name="0.1.3"></a>
# [0.1.3](https://bitbucket.org/riafox/mongodb-bson-types/commits/f3d3056eb54f28438a24d5a81761674380deab6f) (2017-05-02)

### Features
* **ObjectId:** add basic support for ObjetId objects for php mongodb driver versions 1.1.x and 1.2.x, deprecate mongoId
* **UTCDateTime:** add basic support for UTCDateTime objects for php mongodb driver versions 1.1.x and 1.2.x